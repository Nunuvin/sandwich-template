'''
Card generator v 1.0
distributed under GPL
created by Vlad

'''

def interpreter(template_file):
	#loads in the templates |return list of parts of code/words
	data_list=template_file.readlines()
	return data_list

def master_interpreter(master):
	#organizes commands from command script
	#outputs organized list in which x[y] y specifies card and x parameter
	#more elegant solution might exist but i think this one is good too
	master_list=interpreter(master)
	export_filename=[]
	card_template_type=[]
	good_skill=[]
	bad_skill=[]
	name=[]

	for command in master_list:	

		filename_count=0
		card_template_count=0
		instance_line=False
		var_count = 0
		letter_count=0
		good_skill_sentence=[]
		bad_skill_sentence=[]
		name_word=[]

		for letter in command:			
			letter_count+=1
			if instance_line==True:
				if letter!='|' and var_count==0:
					name_word.append(letter)
				elif letter=='|':
					var_count+=1					
				elif letter!='|' and var_count==1:
					good_skill_sentence.append(letter)					
				elif letter!="|" and var_count==2:
					bad_skill_sentence.append(letter)
			elif letter=='[':				
				filename=command[1:-1]
				secondline=False				
				break
			elif letter=='{':								
				template_type=command[1:-1]
				secondline=False
				break
			elif letter=='-':
				
				secondline=True
				instance_line=True
		if instance_line==True:
			name_word=''.join(i.replace('\n',' ') for i in name_word)
			name.append(name_word)
			export_filename.append(''.join(i.replace('\n',' ') for i in filename))
			card_template_type.append(''.join(i.replace('\n',' ') for i in template_type))
			good_skill_sentence=''.join(i.replace('\n',' ') for i in good_skill_sentence)
			good_skill.append(good_skill_sentence)
			bad_skill_sentence=''.join(i.replace('\n',' ') for i in bad_skill_sentence)
			bad_skill.append(bad_skill_sentence[:-1]) #last letter is space which leads to word .
			# and enter can happen there too :/
				
	#so [0] for all lists is 1st card [1] is 2nd card etc.
	master_transcript=[export_filename,card_template_type,name,good_skill,bad_skill]
	#for each export 
	return master_transcript

def card_template_interpreter(card_templates):
	#creates organized list of 2 lists: 1 template names [x] template[x]
	card_list=interpreter(card_templates)
	card_template=[]
	template_name_list=[]
	part_temp_bucket=[]
	template=[]
	templates=[]
	template_bucket=[]
	new_template=False
	for row in card_list:		
		if ''.join(row[0:4])=='<!--': #is it a special line			
			if ''.join(row[4:13])=='template:':	#is it a template line?
				if new_template==True:
					templates.append(template)
					template=[]
					new_template=False
				new_template=True
				i=0				
				for letter in row[13:]: #whats template name?
					if letter=='-' and ''.join(row[13+i:16+i])=='-->':						
						template_name_list.append(''.join(row[13:13+i]))						
						break
					i+=1
			else: #endofpart line it is then:
				template.append(part_temp_bucket)
				part_temp_bucket=[]
		else: #peasant line
			part_temp_bucket.append(row.strip())
		


	card_template.append(template_name_list)
	card_template.append(templates)
	return card_template

def html_template_interpreter(html_templates):
	html_list=interpreter(html_templates) #list of lines exists!
	store_line=[]
	html_part_list=[]
	for line in html_list: #is it a tag?
		if (
			''.join(line[:13])=='<!--header-->' or ''.join(line[:16])=='<!--row_start-->'
			or ''.join(line[:14])=='<!--mid_row-->' or ''.join(line[:14])=='<!--row_end-->'
			or ''.join(line[:13])=='<!--footer-->'
		) :
			html_part_list.append(store_line) #store the code in part list
			store_line=[]
		else: #peasants
			store_line.append(line)
	return html_part_list

def exporter(script_list,card_template_list,html_template_list):
	#inputs:
	#script_list[export_filename,card_template_type,name,good_skill,bad_skill]
	#card_template_list[template_name_list,template1[part list],t2[p],etc]
	#html_template_list[part_list]
	
	unique_file_name_list=[]
	exportfile=[]
	#create all unique export files shall have same [x] as scripts related
	for i in xrange(0,len(script_list[0])):
		for each in script_list[0]:
			uniqueness=True
			for unique in unique_file_name_list:
				if each==unique:
					uniqueness=False
			if uniqueness==True:
				exportfile.append(open(each+'.html','w+'))
				unique_file_name_list.append(each)
				 #write header
				for item in html_template_list[0]:
					exportfile[len(exportfile)-1].write(item)
				break

	#creating unique card for each name:
	count=0
	
	template_number=0
	unique_temp_type=[]
	for template_type in script_list[1]:
		uniqueness=True
		for uni in unique_temp_type:
			
			if template_type==uni:
				uniqueness=False
		if uniqueness==True:
			unique_temp_type.append(template_type)
	#print unique_temp_type

	#print script_list[4]
	for the_name in script_list[2]:
		template_number=0
		file_id=0
		personalized_card=[]
		for uniq in unique_temp_type:
			#print uniq
			script_list[1][count]
			if script_list[1][count]==uniq:
				break
			else:
				template_number+=1
		#print template_number	
		for one in unique_file_name_list:
			if one==script_list[0][count]:
				break
			else:
				file_id+=1

		if count==0 or count%2==0:
			#odd
			for item in html_template_list[1]:
				exportfile[file_id].write(item)#start row


		#HAS TO BE ADJUSTED TO A DIFFERENT TEMPLATE!!!!!!

		personalized_card.append(card_template_list[1][template_number][0])
		personalized_card.append(' '+the_name)
		personalized_card.append(card_template_list[1][template_number][1])
		personalized_card.append(script_list[3][count])
		personalized_card.append(card_template_list[1][template_number][2])
		personalized_card.append(script_list[4][count])
		personalized_card.append(card_template_list[1][template_number][3])

		result = []
		for submember in personalized_card:
			if type(submember) == list:
				for item in submember:
					result.append(item)
			else:
				result.append(submember)
		personalized_card=list(''.join(result))


		#formatting text has to be list of chars though!
		#char per line: 36
		#print personalized_card
		'''for i in xrange(0,len(personalized_card),36): #last one is the space step
			enter_placed=False
			j=0
			while enter_placed==False: #going backwards looking for space to put enter to
				#print personalized_card[i-j]
				if personalized_card[i-j]==' ':
					personalized_card[i-j]='\n'
					enter_placed=True
				else:
					j+=1'''
		max_length=38
		max_new_lines=len(personalized_card)/max_length
		idv=0
		'''print personalized_card
		for i in personalized_card:
			if i=='\n':
				print 'new line found! culprits id is: ', idv
			idv+=1'''
		for i in xrange(0,len(personalized_card)): #text formatting
			if i%max_length==0 and i!=0 and max_new_lines>=0:
				enter_placed=False
				j=0
				while enter_placed==False: #going backwards looking for space to put enter to
					#print personalized_card[i-j]
					if personalized_card[i-j]==' ' :
						personalized_card[i-j]='\n'
						enter_placed=True
						max_new_lines-=1
					else:
						j+=1
			elif max_new_lines<1: break
		jr=0
		#print personalized_card
		'''for i in personalized_card: #quick and dirty last enter fix
			if i=="\n":
				#print 'yey'
				last_new_line=jr
			jr+=1
		personalized_card[last_new_line]=' ' '''

		for item in personalized_card:
			exportfile[file_id].write(item)


		if count==0 or count%2==0:
			for item in html_template_list[2]:
				exportfile[file_id].write(item)#mid row	
		if count%2==1: #even
			for item in html_template_list[3]:
				exportfile[file_id].write(item)#end row	
		count+=1	
	#closes all files:
	for open_exportfile in exportfile:
		for item in html_template_list[4]:
			open_exportfile.write(item)# writes footer
		open_exportfile.close()
	return None

def abrr2text(script,abrr):
	abrr=interpreter(abrr)
	list_abr=[]
	list_meaning=[]
	for j in xrange(0,len(abrr)):
		for i in xrange(0,len(abrr[j])):
			if abrr[j][i]=='-':
				start=i+1
			if abrr[j][i]=='|':
				end=i
		
		list_abr.append(abrr[j][start:end])
		list_meaning.append(abrr[j][end+1:-1])
	#print list_abr,'-abr:mean-',list_meaning
	
	#print list_abr[0]
	#print list_meaning[0]
	#print script

	index=0
	for abr in list_abr:
		for listz in script:
			#print listz
			u=0
			for member in listz:
				if member==abr+' ':
					#print member
					listz[u]=list_meaning[index]
					#print member
				u+=1
			#print listz
		index+=1
	# script
	
	return script

def main():	
	#main
	#opening all files | all of these files has to be included in file_close()
	master=raw_input('Please name the command file (no file extension) ')
	master=open(master+".txt",'r')
	card_templates=open("card_templates.txt","r")
	html_templates=open("html_templates.html","r")
	abbr=raw_input('name of abbreviator used:(def for default) ')
	if abbr=='def':	abbr=open("abbreviate.txt","r")
	else: abbr=open(abbr+'.txt','r')

	script_list=master_interpreter(master)
	#print script_list
	script_list_mod=abrr2text(script_list,abbr)
	card_template_list=card_template_interpreter(card_templates)
	#print card_template_list
	html_template_list=html_template_interpreter(html_templates)
	#print html_template_list
	exporter(script_list_mod,card_template_list,html_template_list)
	#closing all files
	master.close()
	card_templates.close()
	html_templates.close()
	abbr.close()
	return None

main()